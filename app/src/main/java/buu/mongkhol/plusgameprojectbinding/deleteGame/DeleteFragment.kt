package buu.mongkhol.plusgameprojectbinding.deleteGame

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
//import buu.mongkhol.plusgameprojectbinding.DeleteFragmentArgs
//import buu.mongkhol.plusgameprojectbinding.DeleteFragmentDirections
import buu.mongkhol.plusgameprojectbinding.R
import buu.mongkhol.plusgameprojectbinding.databinding.FragmentDeleteBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DeleteFragment : Fragment() {

    private lateinit var binding: FragmentDeleteBinding
    private lateinit var viewModel: DeleteViewModel
    // TODO: Rename and change types of parameters

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentDeleteBinding>(
            inflater,
            R.layout.fragment_delete, container, false
        )

        viewModel = ViewModelProvider(this).get(DeleteViewModel::class.java)
        binding.viewModel = viewModel

        val args =
            DeleteFragmentArgs.fromBundle(
                requireArguments()
            )
        viewModel.setDeleteCorrect(args.deleteCorrect)
        viewModel.setDeleteIncorrect(args.deleteIncorrect)

        viewModel.randomProblem()

        viewModel.evenChoiceButton1.observe(viewLifecycleOwner, Observer { select ->
            if (select) {
                if (viewModel.isCorrect.value!!) {
                    binding.btn1.setBackgroundColor(Color.parseColor("#69FF78"))
                    binding.txtAnswer.setTextColor(Color.parseColor("#69FF78"))
                } else {
                    binding.btn1.setBackgroundColor(Color.parseColor("#FF8848"))
                    binding.txtAnswer.setTextColor(Color.parseColor("#FF8848"))
                }
                binding.invalidateAll()

                Handler().postDelayed({
                    binding.btn1.setBackgroundColor(Color.parseColor("#E4E1E8"))
                    viewModel.setResult("")
                    viewModel.randomProblem()
                    binding.invalidateAll()
                    viewModel.choiceButton1Complete()
                }, 300)
            }
        })

        viewModel.evenChoiceButton2.observe(viewLifecycleOwner, Observer { select ->
            if (select) {
                if (viewModel.isCorrect.value!!) {
                    binding.btn2.setBackgroundColor(Color.parseColor("#69FF78"))
                    binding.txtAnswer.setTextColor(Color.parseColor("#69FF78"))
                } else {
                    binding.btn2.setBackgroundColor(Color.parseColor("#FF8848"))
                    binding.txtAnswer.setTextColor(Color.parseColor("#FF8848"))
                }
                binding.invalidateAll()

                Handler().postDelayed({
                    binding.btn2.setBackgroundColor(Color.parseColor("#E4E1E8"))
                    viewModel.setResult("")
                    viewModel.randomProblem()
                    binding.invalidateAll()
                    viewModel.choiceButton2Complete()
                }, 300)
            }
        })
        viewModel.evenChoiceButton3.observe(viewLifecycleOwner, Observer { select ->
            if (select) {
                if (viewModel.isCorrect.value!!) {
                    binding.btn3.setBackgroundColor(Color.parseColor("#69FF78"))
                    binding.txtAnswer.setTextColor(Color.parseColor("#69FF78"))
                } else {
                    binding.btn3.setBackgroundColor(Color.parseColor("#FF8848"))
                    binding.txtAnswer.setTextColor(Color.parseColor("#FF8848"))
                }
                binding.invalidateAll()

                Handler().postDelayed({
                    binding.btn3.setBackgroundColor(Color.parseColor("#E4E1E8"))
                    viewModel.setResult("")
                    viewModel.randomProblem()
                    binding.invalidateAll()
                    viewModel.choiceButton3Complete()
                }, 300)
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            val navControler = this@DeleteFragment.findNavController()
            navControler.navigate(
                DeleteFragmentDirections.actionDeleteFragmentToGameFragmentTitle(
                    viewModel.deleteCorrect.value!!,
                    viewModel.deleteIncorrect.value!!
                )
            )
        }
        return binding.root
    }

}