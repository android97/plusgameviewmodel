package buu.mongkhol.plusgameprojectbinding.plusGame

import android.os.Handler
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class PlusViewModel : ViewModel() {
    private var _plusCorrect = MutableLiveData<Int>()
    val plusCorrect: LiveData<Int>
        get() = _plusCorrect

    private var _plusIncorrect = MutableLiveData<Int>()
    val plusIncorrect: LiveData<Int>
        get() = _plusIncorrect

    private var _numberFirstPlus = MutableLiveData<Int>()
    val numberFirstPlus: LiveData<Int>
        get() = _numberFirstPlus

    private var _numberSeconPlus = MutableLiveData<Int>()
    val numberSeconPlus: LiveData<Int>
        get() = _numberSeconPlus

    private var _answer = MutableLiveData<Int>()
    val answer: LiveData<Int>
        get() = _answer

    private var _result = MutableLiveData<String>()
    val result: LiveData<String>
        get() = _result

    private var _choice1 = MutableLiveData<Int>()
    val choice1: LiveData<Int>
        get() = _choice1

    private var _choice2 = MutableLiveData<Int>()
    val choice2: LiveData<Int>
        get() = _choice2

    private var _choice3 = MutableLiveData<Int>()
    val choice3: LiveData<Int>
        get() = _choice3

    private var _evenChoiceButton1 = MutableLiveData<Boolean>()
    val evenChoiceButton1: LiveData<Boolean>
        get() = _evenChoiceButton1

    private var _evenChoiceButton2 = MutableLiveData<Boolean>()
    val evenChoiceButton2: LiveData<Boolean>
        get() = _evenChoiceButton2

    private var _evenChoiceButton3 = MutableLiveData<Boolean>()
    val evenChoiceButton3: LiveData<Boolean>
        get() = _evenChoiceButton3

    private var _isCorrect = MutableLiveData<Boolean>()
    val isCorrect: LiveData<Boolean>
        get() = _isCorrect

    fun setPlusCorrect(correct: Int) {
        _plusCorrect.value = correct
    }

    fun setPlusIncorrect(incorrect: Int) {
        _plusIncorrect.value = incorrect
    }

    fun setResult(result: String) {
        _result.value = result
    }

    fun randomProblem() {
        _numberFirstPlus.value = Random.nextInt(0, 11)
        _numberSeconPlus.value = Random.nextInt(0, 11)
        Log.i("Meaw", "First ${numberFirstPlus}")
        Log.i("Meaw", "Seccond ${numberSeconPlus}")
        _answer.value = _numberFirstPlus.value!! + _numberSeconPlus.value!!

        randomAnswer()
    }

    private fun randomAnswer() {
        val answers: Int = Random.nextInt(0, 3)
        if (answers == 0) {
            _choice1.value = (_answer.value?.plus(0))
            _choice2.value = (_answer.value?.plus(5))
            _choice3.value = (_answer.value?.plus(2))
        }
        if (answers == 1) {
            _choice1.value = (_answer.value?.minus(1))
            _choice2.value = (_answer.value?.plus(0))
            _choice3.value = (_answer.value?.plus(2))
        }
        if (answers == 2) {
            _choice1.value = (_answer.value?.minus(2))
            _choice2.value = (_answer.value?.plus(1))
            _choice3.value = (_answer.value?.plus(0))
        }
    }


    private fun checkButton(choice: Int) {
        val choiceSelect = when (choice) {
            1 -> _choice1.value
            2 -> _choice2.value
            3 -> _choice3.value
            else -> -1
        }
        Log.i("maew", "before ${_answer.value}")
        Log.i("maew", "before ${choiceSelect}")
        if (_answer.value == choiceSelect) {
//            Log.i("maew","------Test------")
//            Log.i("maew","before ${_plusCorrect.value }")
            _plusCorrect.value = _plusCorrect.value?.plus(1)
//            Log.i("maew","aftter ${_plusCorrect.value }")
            _result.value = "ถูกต้อง"
            _isCorrect.value = true
        } else {
            _plusIncorrect.value = _plusIncorrect.value?.plus(1)
            _result.value = "ไม่ถูกนะ"
            _isCorrect.value = false
        }

    }

    fun choiceButton1() {
//        Log.i("maew","------TestJa------")
        checkButton(1)
        _evenChoiceButton1.value = true
    }

    fun choiceButton2() {
        checkButton(2)
        _evenChoiceButton2.value = true
    }

    fun choiceButton3() {
        checkButton(3)
        _evenChoiceButton3.value = true
    }

    fun choiceButton1Complete() {
        _evenChoiceButton1.value = false
    }

    fun choiceButton2Complete() {
        _evenChoiceButton2.value = false
    }

    fun choiceButton3Complete() {
        _evenChoiceButton2.value = false
    }

    init {
        _plusCorrect.value = 0
        _plusIncorrect.value = 0
    }
}