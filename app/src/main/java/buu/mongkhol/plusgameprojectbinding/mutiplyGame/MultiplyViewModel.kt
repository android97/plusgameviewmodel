package buu.mongkhol.plusgameprojectbinding.mutiplyGame

import android.os.Handler
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class MultiplyViewModel : ViewModel() {

    private var _multiplyCorrect = MutableLiveData<Int>()
    val multiplyCorrect: LiveData<Int>
        get() = _multiplyCorrect

    private var _multiplyIncorrect = MutableLiveData<Int>()
    val multiplyIncorrect: LiveData<Int>
        get() = _multiplyIncorrect

    private var _numberFirstMultiply = MutableLiveData<Int>()
    val numberFirstMultiply: LiveData<Int>
        get() = _numberFirstMultiply

    private var _numberSeconMultiply = MutableLiveData<Int>()
    val numberSeconMultiply: LiveData<Int>
        get() = _numberSeconMultiply

    private var _answer = MutableLiveData<Int>()
    val answer: LiveData<Int>
        get() = _answer

    private var _result = MutableLiveData<String>()
    val result: LiveData<String>
        get() = _result

    private var _choice1 = MutableLiveData<Int>()
    val choice1: LiveData<Int>
        get() = _choice1

    private var _choice2 = MutableLiveData<Int>()
    val choice2: LiveData<Int>
        get() = _choice2

    private var _choice3 = MutableLiveData<Int>()
    val choice3: LiveData<Int>
        get() = _choice3

    private var _evenChoiceButton1 = MutableLiveData<Boolean>()
    val evenChoiceButton1: LiveData<Boolean>
        get() = _evenChoiceButton1

    private var _evenChoiceButton2 = MutableLiveData<Boolean>()
    val evenChoiceButton2: LiveData<Boolean>
        get() = _evenChoiceButton2

    private var _evenChoiceButton3 = MutableLiveData<Boolean>()
    val evenChoiceButton3: LiveData<Boolean>
        get() = _evenChoiceButton3

    private var _isCorrect = MutableLiveData<Boolean>()
    val isCorrect: LiveData<Boolean>
        get() = _isCorrect

    fun setMultiplyCorrect(correct: Int) {
        _multiplyCorrect.value = correct
    }

    fun setMultiplyIncorrect(correct: Int) {
        _multiplyIncorrect.value = correct
    }

    fun setResult(result: String) {
        _result.value = result
    }

    fun randomProblem() {
        _numberFirstMultiply.value = Random.nextInt(0, 11)
        _numberSeconMultiply.value = Random.nextInt(0, 11)
        Log.i("Meaw", "First ${numberFirstMultiply}")
        Log.i("Meaw", "Seccond ${numberSeconMultiply}")
        _answer.value = _numberFirstMultiply.value!! * _numberSeconMultiply.value!!

        randomAnswer()
    }

    private fun randomAnswer() {
        val answers: Int = Random.nextInt(0, 3)
        if (answers == 0) {
            _choice1.value = (_answer.value?.plus(0))
            _choice2.value = (_answer.value?.plus(5))
            _choice3.value = (_answer.value?.plus(2))
        }
        if (answers == 1) {
            _choice1.value = (_answer.value?.minus(1))
            _choice2.value = (_answer.value?.plus(0))
            _choice3.value = (_answer.value?.plus(2))
        }
        if (answers == 2) {
            _choice1.value = (_answer.value?.minus(2))
            _choice2.value = (_answer.value?.plus(1))
            _choice3.value = (_answer.value?.plus(0))
        }
    }

    private fun checkButton(choice: Int) {
        val choiceSelect = when (choice) {
            1 -> _choice1.value
            2 -> _choice2.value
            3 -> _choice3.value
            else -> -1
        }
        if (_answer.value == choiceSelect) {
            _multiplyCorrect.value = _multiplyCorrect.value?.plus(1)
            _result.value = "ถูกต้อง"
            _isCorrect.value = true
        } else {
            _multiplyIncorrect.value = _multiplyIncorrect.value?.plus(1)
            _result.value = "ไม่ถูกนะ"
            _isCorrect.value = false
        }
    }

    fun choiceButton1() {
        checkButton(1)
        _evenChoiceButton1.value = true
    }

    fun choiceButton2() {
        checkButton(2)
        _evenChoiceButton2.value = true
    }

    fun choiceButton3() {
        checkButton(3)
        _evenChoiceButton3.value = true
    }

    fun choiceButton1Complete() {
        _evenChoiceButton1.value = false
    }

    fun choiceButton2Complete() {
        _evenChoiceButton2.value = false
    }

    fun choiceButton3Complete() {
        _evenChoiceButton2.value = false
    }

    init {
        _multiplyCorrect.value = 0
        _multiplyIncorrect.value = 0
    }

}