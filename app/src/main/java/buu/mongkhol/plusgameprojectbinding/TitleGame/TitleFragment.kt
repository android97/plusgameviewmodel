package buu.mongkhol.plusgameprojectbinding.TitleGame

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import buu.mongkhol.plusgameprojectbinding.R
import buu.mongkhol.plusgameprojectbinding.databinding.FragmentTitleBinding

class TitleFragment : Fragment() {

    private var plusCorrect: Int = 0
    private var plusIncorrect: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(
            inflater,
            R.layout.fragment_title,
            container,
            false
        )

        binding.btnPlusGame.setOnClickListener { view: View ->
            val navControler = this@TitleFragment.findNavController()
            navControler.navigate(
                TitleFragmentDirections.actionGameFragmentTitleToPlusFragment(
                    plusCorrect,
                    plusIncorrect
                )
            )
        }
        binding.btnDeleteGame.setOnClickListener { view: View ->
            val navControler = this@TitleFragment.findNavController()
            navControler.navigate(
                TitleFragmentDirections.actionGameFragmentTitleToDeleteFragment(
                    plusCorrect,
                    plusIncorrect
                )
            )
        }
        binding.btnMultiplyGame.setOnClickListener { view: View ->
            val navControler = this@TitleFragment.findNavController()
            navControler.navigate(
                TitleFragmentDirections.actionGameFragmentTitleToMultiplyFragment(
                    plusCorrect,
                    plusIncorrect
                )
            )
        }
        setHasOptionsMenu(true)

        val args = TitleFragmentArgs.fromBundle(requireArguments())
        plusCorrect = args.plusCorrect
        plusIncorrect = args.plusIncorrect
        binding.plusCorrect = plusCorrect
        binding.plusIncorrect = plusIncorrect

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.navdrawer_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item!!,
            view!!.findNavController()
        ) || super.onOptionsItemSelected(item)
    }
}
